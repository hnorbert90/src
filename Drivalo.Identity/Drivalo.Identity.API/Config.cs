﻿using System.Collections.Generic;

using IdentityServer4.Models;

namespace Drivalo.Identification.API
{
    public class Config
    {
        public static IEnumerable<ApiResource> Apis =>
           new List<ApiResource>
           {
                new ApiResource("user_service_api", "User service API")
           };

        public static IEnumerable<Client> Clients =>
            new List<Client>
            {
                new Client
                {
                    ClientId = "vue_desktop",

                    // no interactive user, use the clientid/secret for authentication
                    AllowedGrantTypes = GrantTypes.ClientCredentials,

                    // secret for authentication
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },

                    // scopes that client has access to
                    AllowedScopes = { "user_service_api" }
                }
            };
    }
}
