﻿namespace Drivalo.Common.Domain
{
    public class EntityBase<TPrimaryKey>
    {
        public virtual TPrimaryKey ID { get; set; } = default!;
    }
}