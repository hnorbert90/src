﻿namespace Drivalo.User.Persistence
{
    using Drivalo.User.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(user => user.ID);
            builder.HasIndex(user => new { user.UserName, user.Email }).IsUnique();
            builder.Property(user => user.ID).ValueGeneratedOnAdd();
            builder.Property(user => user.UserName).HasMaxLength(50);
            builder.Property(user => user.Password).HasMaxLength(100);
            builder.Property(user => user.Email).HasMaxLength(255);
            builder.Property(user => user.LastLogin);
            builder.Property(user => user.Online);
            builder.Property(user => user.PrivacyPolicyAccepted);
            builder.Property(user => user.VerifiedEmail);
            builder.Property(user => user.PersonalDataID);
            builder.Property(user => user.Created);
            builder.Property(user => user.Modified);

            builder.HasOne(user => user.PersonalData).WithOne().HasForeignKey<User>(user => user.PersonalDataID);
        }
    }
}