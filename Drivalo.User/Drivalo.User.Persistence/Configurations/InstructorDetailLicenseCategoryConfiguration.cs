﻿namespace Drivalo.User.Persistence
{
    using Drivalo.User.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class InstructorDetailLicenseCategoryConfiguration : IEntityTypeConfiguration<InstructorDetailLicenseCategory>
    {
        public void Configure(EntityTypeBuilder<InstructorDetailLicenseCategory> builder)
        {
            builder.HasKey(entity => entity.ID);
            builder.Property(entity => entity.ID).ValueGeneratedOnAdd();
            builder.Property(entity => entity.InstructorDetailID);


            builder.HasOne(entity => entity.LicenseCategory).WithMany().HasForeignKey(entity => entity.InstructorDetailID);
        }
    }
}