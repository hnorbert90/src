﻿namespace Drivalo.User.Persistence
{
    using Drivalo.User.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class VehicleConfiguration : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            builder.HasKey(entity => entity.ID);
            builder.Property(entity => entity.ID).ValueGeneratedOnAdd();
            builder.Property(entity => entity.Model);
            builder.Property(entity => entity.Year);

            builder.OwnsOne(entity => entity.ShifterType);
            builder.OwnsOne(entity => entity.FuelType);

            builder.HasMany(entity => entity.VehicleFeatures).WithOne(entity => entity.Vehicle!).HasForeignKey(entity => entity.VehicleFeatureID);
        }
    }
}