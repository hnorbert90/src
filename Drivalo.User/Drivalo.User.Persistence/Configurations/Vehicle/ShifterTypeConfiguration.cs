﻿namespace Drivalo.User.Persistence
{
    using Drivalo.User.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ShifterTypeConfiguration : IEntityTypeConfiguration<ShifterType>
    {
        public void Configure(EntityTypeBuilder<ShifterType> builder)
        {
            builder.HasKey(entity => entity.ID);
            builder.HasIndex(entity => entity.Name).IsUnique();
            builder.Property(entity => entity.ID).ValueGeneratedOnAdd();
            builder.Property(entity => entity.Name);
        }
    }
}