﻿namespace Drivalo.User.Persistence
{
    using Drivalo.User.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class VehicleVehicleFeatureConfiguration : IEntityTypeConfiguration<VehicleVehicleFeature>
    {
        public void Configure(EntityTypeBuilder<VehicleVehicleFeature> builder)
        {
            builder.HasKey(entity => entity.ID);
            builder.Property(entity => entity.ID).ValueGeneratedOnAdd();
            builder.Property(entity => entity.VehicleFeatureID);
            builder.Property(entity => entity.VehicleID);

            builder.HasOne(entity => entity.Vehicle).WithMany(entity => entity!.VehicleFeatures).HasForeignKey(entity => entity.VehicleID);
            builder.HasOne(entity => entity.VehicleFeature).WithMany().HasForeignKey(entity => entity.VehicleFeatureID);
        }
    }
}