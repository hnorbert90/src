﻿namespace Drivalo.User.Persistence
{
    using Drivalo.User.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class UserLanguageConfiguration : IEntityTypeConfiguration<UserLanguage>
    {
        public void Configure(EntityTypeBuilder<UserLanguage> builder)
        {
            builder.HasKey(entity => entity.ID);
            builder.Property(entity => entity.ID).ValueGeneratedOnAdd();
            builder.Property(entity => entity.UserID);
            builder.Property(entity => entity.LanguageID);

            builder.HasOne(entity => entity.User).WithMany().HasForeignKey(entity => entity.UserID);
            builder.HasOne(entity => entity.Language).WithMany().HasForeignKey(entity => entity.LanguageID);
        }
    }
}