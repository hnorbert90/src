﻿namespace Drivalo.User.Persistence
{
    using Drivalo.User.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class InstructorDetailConfiguration : IEntityTypeConfiguration<InstructorDetail>
    {
        public void Configure(EntityTypeBuilder<InstructorDetail> builder)
        {
            builder.HasKey(entity => entity.ID);
            builder.Property(entity => entity.ID).ValueGeneratedOnAdd();
            builder.Property(entity => entity.UserID);
            builder.Property(entity => entity.Introduction);

            builder.HasMany(entity => entity.LicenseCategories).WithOne().HasForeignKey(entity => entity.InstructorDetailID);
            builder.OwnsMany(entity => entity.Vehicles);
        }
    }
}