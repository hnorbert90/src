﻿namespace Drivalo.User.Domain
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ContactConfiguration : IEntityTypeConfiguration<Contact>
    {
        public void Configure(EntityTypeBuilder<Contact> builder)
        {
            builder.HasKey(entity => entity.ID);
            builder.Property(entity => entity.ID).ValueGeneratedOnAdd();
            builder.Property(entity => entity.Phone1);
            builder.Property(entity => entity.Phone2);
            builder.Property(entity => entity.Phone3);
            builder.Property(entity => entity.UserID);
            builder.Property(entity => entity.WebsiteURL);
            builder.Property(entity => entity.FacebookID);
        }
    }
}