﻿namespace Drivalo.User.Persistence
{
    using Drivalo.User.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class PersonalDataConfiguration : IEntityTypeConfiguration<PersonalData>
    {
        public void Configure(EntityTypeBuilder<PersonalData> builder)
        {
            builder.HasKey(personalData => personalData.ID);
            builder.Property(personalData => personalData.ID).ValueGeneratedOnAdd();
            builder.Property(personalData => personalData.AvatarURL).HasMaxLength(255);
            builder.Property(personalData => personalData.BirthDay);
            builder.Property(personalData => personalData.Title).HasMaxLength(20);
            builder.Property(personalData => personalData.FirstName).HasMaxLength(50);
            builder.Property(personalData => personalData.LastName).HasMaxLength(50);
            builder.Property(personalData => personalData.Profession).HasMaxLength(100);
            builder.Property(personalData => personalData.PrimaryLanguage).HasMaxLength(50);
        }
    }
}