﻿namespace Drivalo.User.Persistence
{
    using Drivalo.User.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class LicenseCategoryConfiguration : IEntityTypeConfiguration<LicenseCategory>
    {
        public void Configure(EntityTypeBuilder<LicenseCategory> builder)
        {
            builder.HasKey(licenseCategory => licenseCategory.ID);
            builder.Property(licenseCategory => licenseCategory.ID).ValueGeneratedOnAdd();

            builder.Property(licenseCategory => licenseCategory.Name).IsUnicode();
        }
    }
}