﻿namespace Drivalo.User.Persistence
{
    using Drivalo.User.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class PersonalDataLicenseCategoryConfiguration : IEntityTypeConfiguration<PersonalDataLicenseCategory>
    {
        public void Configure(EntityTypeBuilder<PersonalDataLicenseCategory> builder)
        {
            builder.HasIndex(t => new { t.PersonalDataID, t.LicenseCategoryID });

            builder.HasKey(personalDataLicenseCategory => personalDataLicenseCategory.ID);
            builder.Property(personalDataLicenseCategory => personalDataLicenseCategory.ID).ValueGeneratedOnAdd();

            builder.Property(personalDataLicenseCategory => personalDataLicenseCategory.PersonalDataID);
            builder.Property(personalDataLicenseCategory => personalDataLicenseCategory.LicenseCategoryID);

            builder.HasOne(personalDataLicenseCategory => personalDataLicenseCategory.PersonalData).WithMany(productData => productData!.PersonalDataLicenseCategories).HasForeignKey(personalDataLicenseCategory => personalDataLicenseCategory.PersonalDataID);
            builder.HasOne(personalDataLicenseCategory => personalDataLicenseCategory.LicenseCategory).WithMany().HasForeignKey(personalDataLicenseCategory => personalDataLicenseCategory.LicenseCategoryID);
        }
    }
}