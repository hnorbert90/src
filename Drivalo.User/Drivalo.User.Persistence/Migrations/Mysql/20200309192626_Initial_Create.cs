﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Drivalo.User.Persistence.Migrations.MySQL
{
    public partial class Initial_Create : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FuelTypes",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FuelTypes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Language",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Language", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "LicenseCategories",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LicenseCategories", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "ShifterTypes",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShifterTypes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "VehicleFeatures",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleFeatures", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "PersonalData",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AvatarURL = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    Profession = table.Column<string>(nullable: true),
                    BirthDay = table.Column<DateTime>(nullable: true),
                    PrimaryLanguageID = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonalData", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PersonalData_Language_PrimaryLanguageID",
                        column: x => x.PrimaryLanguageID,
                        principalTable: "Language",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PersonalDataLicenseCategory",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    PersonalDataID = table.Column<long>(nullable: false),
                    LicenseCategoryID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonalDataLicenseCategory", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PersonalDataLicenseCategory_LicenseCategories_LicenseCategor~",
                        column: x => x.LicenseCategoryID,
                        principalTable: "LicenseCategories",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PersonalDataLicenseCategory_PersonalData_PersonalDataID",
                        column: x => x.PersonalDataID,
                        principalTable: "PersonalData",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: false),
                    VerifiedEmail = table.Column<bool>(nullable: false),
                    PrivacyPolicyAccepted = table.Column<bool>(nullable: false),
                    Online = table.Column<bool>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: true),
                    LastLogin = table.Column<DateTime>(nullable: true),
                    PersonalDataID = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Users_PersonalData_PersonalDataID",
                        column: x => x.PersonalDataID,
                        principalTable: "PersonalData",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InstructorDetails",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserID = table.Column<long>(nullable: false),
                    Introduction = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstructorDetails", x => x.ID);
                    table.ForeignKey(
                        name: "FK_InstructorDetails_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InstructorDetailLicenseCategory",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    InstructorDetailID = table.Column<long>(nullable: false),
                    LicenseCategoryID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstructorDetailLicenseCategory", x => x.ID);
                    table.ForeignKey(
                        name: "FK_InstructorDetailLicenseCategory_InstructorDetails_Instructor~",
                        column: x => x.InstructorDetailID,
                        principalTable: "InstructorDetails",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InstructorDetailLicenseCategory_LicenseCategories_LicenseCat~",
                        column: x => x.LicenseCategoryID,
                        principalTable: "LicenseCategories",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Vehicles",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Year = table.Column<int>(nullable: false),
                    Model = table.Column<string>(nullable: false),
                    ShifterTypeID = table.Column<int>(nullable: false),
                    FuelTypeID = table.Column<int>(nullable: false),
                    InstructorDetailID = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicles", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Vehicles_FuelTypes_FuelTypeID",
                        column: x => x.FuelTypeID,
                        principalTable: "FuelTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Vehicles_InstructorDetails_InstructorDetailID",
                        column: x => x.InstructorDetailID,
                        principalTable: "InstructorDetails",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vehicles_ShifterTypes_ShifterTypeID",
                        column: x => x.ShifterTypeID,
                        principalTable: "ShifterTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleVehicleFeature",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    VehicleFeatureID = table.Column<int>(nullable: false),
                    VehicleID = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleVehicleFeature", x => x.ID);
                    table.ForeignKey(
                        name: "FK_VehicleVehicleFeature_VehicleFeatures_VehicleFeatureID",
                        column: x => x.VehicleFeatureID,
                        principalTable: "VehicleFeatures",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VehicleVehicleFeature_Vehicles_VehicleID",
                        column: x => x.VehicleID,
                        principalTable: "Vehicles",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InstructorDetailLicenseCategory_InstructorDetailID",
                table: "InstructorDetailLicenseCategory",
                column: "InstructorDetailID");

            migrationBuilder.CreateIndex(
                name: "IX_InstructorDetailLicenseCategory_LicenseCategoryID",
                table: "InstructorDetailLicenseCategory",
                column: "LicenseCategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_InstructorDetails_UserID",
                table: "InstructorDetails",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_PersonalData_PrimaryLanguageID",
                table: "PersonalData",
                column: "PrimaryLanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_PersonalDataLicenseCategory_LicenseCategoryID",
                table: "PersonalDataLicenseCategory",
                column: "LicenseCategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_PersonalDataLicenseCategory_PersonalDataID",
                table: "PersonalDataLicenseCategory",
                column: "PersonalDataID");

            migrationBuilder.CreateIndex(
                name: "IX_Users_PersonalDataID",
                table: "Users",
                column: "PersonalDataID");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_FuelTypeID",
                table: "Vehicles",
                column: "FuelTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_InstructorDetailID",
                table: "Vehicles",
                column: "InstructorDetailID");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_ShifterTypeID",
                table: "Vehicles",
                column: "ShifterTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleVehicleFeature_VehicleFeatureID",
                table: "VehicleVehicleFeature",
                column: "VehicleFeatureID");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleVehicleFeature_VehicleID",
                table: "VehicleVehicleFeature",
                column: "VehicleID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InstructorDetailLicenseCategory");

            migrationBuilder.DropTable(
                name: "PersonalDataLicenseCategory");

            migrationBuilder.DropTable(
                name: "VehicleVehicleFeature");

            migrationBuilder.DropTable(
                name: "LicenseCategories");

            migrationBuilder.DropTable(
                name: "VehicleFeatures");

            migrationBuilder.DropTable(
                name: "Vehicles");

            migrationBuilder.DropTable(
                name: "FuelTypes");

            migrationBuilder.DropTable(
                name: "InstructorDetails");

            migrationBuilder.DropTable(
                name: "ShifterTypes");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "PersonalData");

            migrationBuilder.DropTable(
                name: "Language");
        }
    }
}
