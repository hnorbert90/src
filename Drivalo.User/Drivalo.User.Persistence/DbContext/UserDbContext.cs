﻿namespace Drivalo.User.Persistence
{
    using Drivalo.User.Domain;

    using Microsoft.EntityFrameworkCore;

    public class UserDbContext : DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Vehicle> Vehicles { get; set; } = null!;
        public DbSet<ShifterType> ShifterTypes { get; set; } = null!;
        public DbSet<VehicleFeature> VehicleFeatures { get; set; } = null!;
        public DbSet<FuelType> FuelTypes { get; set; } = null!;
        public DbSet<User> Users { get; set; } = null!;
        public DbSet<PersonalData> PersonalData { get; set; } = null!;
        public DbSet<LicenseCategory> LicenseCategories { get; set; } = null!;
        public DbSet<InstructorDetail> InstructorDetails { get; set; } = null!;
        public DbSet<Address> Addresses { get; set; } = null!;
        public DbSet<Contact> Contacts { get; set; } = null!;
        public DbSet<Language> Languages { get; set; } = null!;
    }
}