﻿namespace Drivalo.User.Persistence
{
    public enum Provider
    {
        MySql,
        SQLite
    }
}