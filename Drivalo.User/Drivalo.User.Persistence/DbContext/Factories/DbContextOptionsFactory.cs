﻿namespace Drivalo.User.Persistence
{
    using System;

    using Microsoft.EntityFrameworkCore;

    public class DbContextOptionsFactory<TDbContext> where TDbContext : DbContext
    {
        private readonly IDatabaseSettings settings;

        public DbContextOptionsFactory(IDatabaseSettings settings)
        {
            this.settings = settings;
        }

        public DbContextOptions<TDbContext> Create()
        {
            DbContextOptionsBuilder<TDbContext> factory = new DbContextOptionsBuilder<TDbContext>();
            return settings.Provider switch
            {
                Provider.MySql => factory.UseMySql(settings.ConnectionString).Options,
                Provider.SQLite => factory.UseSqlite(settings.ConnectionString).Options,
                _ => throw new ArgumentException($"Unsupported provider: {settings.Provider}"),
            };
        }
    }
}