﻿namespace Drivalo.User.Persistence
{
    using System;

    using Drivalo.User.Domain;

    public class SeedDB
    {
        private readonly UserDbContext uow;

        public SeedDB(UserDbContext uow) => this.uow = uow;

        public void SeedUser()
        {
            var admin = new User
            {
                Created = DateTime.Now,
                Modified = DateTime.Now,
                Email = "admin@drivalo.com",
                UserName = "System",
                Password = "pasd82ksad"
            };

            var user = new User()
            {
                Created = DateTime.Now,
                Modified = DateTime.Now,
                Email = "syrixion@gmail.com",
                UserName = "syrixion",
                Password = "nxsGqkp",
                PersonalData = new PersonalData
                {
                    AvatarURL = "img/profile.jpg",
                    BirthDay = new DateTime(1990, 01, 23),
                    FirstName = "Jasker",
                    LastName = "Péter",
                    Profession = "Gépészmérnők",
                    Title = "Dr.",
                }
            };

            var instructor = new User()
            {
                Created = DateTime.Now.AddDays(-50),
                Modified = DateTime.Now.AddDays(-30),
                Email = "instructor@gmail.com",
                UserName = "kisGza",
                Password = "1esda3332",
                PersonalData = new PersonalData
                {
                    AvatarURL = "img/instructor.jpg",
                    BirthDay = new DateTime(1990, 01, 23),
                    FirstName = "Kis",
                    LastName = "Géza",
                    Profession = "Vezetés oktató",
                }
            };

            var vehicle = new Vehicle
            {
                FuelType = new FuelType { Name = "Diesel" },
                Model = "Audi A4",
                ShifterType = new ShifterType { Name = "Manuális 5 fokozat" },
                VehicleFeatures = new[] { new VehicleVehicleFeature { VehicleFeature = new VehicleFeature { Name = "Klíma" } }, new VehicleVehicleFeature { VehicleFeature = new VehicleFeature { Name = "Oktánméter" } } },
                Year = 2007
            };

            var instructorDetil = new InstructorDetail { Introduction = "Ez lennék én.", User = instructor, Vehicles = new[] { vehicle }, LicenseCategories = new[] { new InstructorDetailLicenseCategory { LicenseCategory = new LicenseCategory { Name = "B" } } } };

            uow.InstructorDetails.Add(instructorDetil);
            uow.Users.AddRange(new[] { admin, instructor, user });
            uow.SaveChanges();
        }
    }
}