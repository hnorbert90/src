﻿namespace Drivalo.User.Persistence
{
    public interface IDatabaseSettings
    {
        string ConnectionString { get; }
        
        Provider Provider { get; }
    }
}
