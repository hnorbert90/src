﻿namespace Drivalo.User.Persistence
{
    using Microsoft.Extensions.Configuration;

    public class DatabaseSettings : IDatabaseSettings
    {
        public DatabaseSettings(IConfiguration configuration)
        {
            ConnectionString = configuration.GetSection("DataBaseSettings").GetValue<string>("ConnectionString");
            Provider = configuration.GetSection("DataBaseSettings").GetValue<Provider>("Provider");
        }

        public string ConnectionString { get; set; }

        public Provider Provider { get; set; }
    }
}