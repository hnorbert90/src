﻿namespace Drivalo.User.Domain
{
    using Drivalo.Common.Domain;

    public class Address : EntityBase<long>
    {
        public virtual long UserID { get; set; }
        public virtual string City { get; set; } = default!;
        public virtual int ZipCode { get; set; }
    }
}