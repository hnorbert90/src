﻿namespace Drivalo.User.Domain
{
    using Drivalo.Common.Domain;

    public class LicenseCategory : EntityBase<int>
    {
        public string Name { get; set; } = default!;
    }
}