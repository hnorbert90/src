﻿namespace Drivalo.User.Domain
{
    using Drivalo.Common.Domain;

    public class InstructorDetailLicenseCategory : EntityBase<long>
    {
        public virtual long InstructorDetailID { get; set; }
        public virtual LicenseCategory? LicenseCategory { get; set; }
    }
}