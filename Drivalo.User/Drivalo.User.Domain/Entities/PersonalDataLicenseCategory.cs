﻿namespace Drivalo.User.Domain
{
    using Drivalo.Common.Domain;

    public class PersonalDataLicenseCategory : EntityBase<long>
    {
        public virtual long PersonalDataID { get; set; }
        public virtual int LicenseCategoryID { get; set; }
        public virtual PersonalData? PersonalData { get; set; }
        public virtual LicenseCategory? LicenseCategory { get; set; }
    }
}