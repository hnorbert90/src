﻿namespace Drivalo.User.Domain
{
    using System;
    using System.Collections.Generic;

    using Drivalo.Common.Domain;

    public class PersonalData : EntityBase<long>
    {
        public virtual string? AvatarURL { get; set; }
        public virtual string? Title { get; set; }
        public virtual string FirstName { get; set; } = default!;
        public virtual string LastName { get; set; } = default!;
        public virtual string? Profession { get; set; }
        public virtual DateTime? BirthDay { get; set; }
        public virtual Language? PrimaryLanguage { get; set; }
        public virtual IEnumerable<PersonalDataLicenseCategory>? PersonalDataLicenseCategories { get; set; }
    }
}