﻿namespace Drivalo.User.Domain
{
    using System.Collections.Generic;

    using Drivalo.Common.Domain;

    public class InstructorDetail : EntityBase<long>
    {
        public virtual long UserID { get; set; }
        public virtual string? Introduction { get; set; }

        public IEnumerable<InstructorDetailLicenseCategory>? LicenseCategories { get; set; }
        public IEnumerable<Vehicle>? Vehicles { get; set; }
        public User? User { get; set; }
    }
}