﻿namespace Drivalo.User.Domain
{
    using Drivalo.Common.Domain;

    public class UserLanguage : EntityBase<long>
    {
        public virtual long UserID { get; set; }
        public virtual long LanguageID { get; set; }

        public virtual User? User { get; set; }
        public virtual Language? Language { get; set; }
    }
}