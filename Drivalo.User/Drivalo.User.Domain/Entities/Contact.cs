﻿namespace Drivalo.User.Domain
{
    using Drivalo.Common.Domain;

    public class Contact : EntityBase<long>
    {
        public virtual long UserID { get; set; }
        public virtual string? Phone1 { get; set; }
        public virtual string? Phone2 { get; set; }
        public virtual string? Phone3 { get; set; }
        public virtual string? FacebookID { get; set; }
        public virtual string? WebsiteURL { get; set; }
    }
}