﻿namespace Drivalo.User.Domain
{
    using System;

    using Drivalo.Common.Domain;

    public class User : EntityBase<long>
    {
        public virtual string UserName { get; set; } = default!;
        public virtual string Email { get; set; } = default!;
        public virtual string Password { get; set; } = default!;
        public virtual bool VerifiedEmail { get; set; }
        public virtual bool PrivacyPolicyAccepted { get; set; }
        public virtual bool Online { get; set; }
        public virtual DateTime Created { get; set; }
        public virtual DateTime? Modified { get; set; }
        public virtual DateTime? LastLogin { get; set; }
        public virtual long? PersonalDataID { get; set; }
        public virtual PersonalData? PersonalData { get; set; }
    }
}