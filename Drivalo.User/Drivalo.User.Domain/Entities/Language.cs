﻿namespace Drivalo.User.Domain
{
    using Drivalo.Common.Domain;

    public class Language : EntityBase<long>
    {
        public virtual string Name { get; set; } = default!;
    }
}