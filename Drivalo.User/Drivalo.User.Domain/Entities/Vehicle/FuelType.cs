﻿namespace Drivalo.User.Domain
{
    using Drivalo.Common.Domain;

    public class FuelType : EntityBase<int>
    {
        public virtual string Name { get; set; } = default!;
    }
}