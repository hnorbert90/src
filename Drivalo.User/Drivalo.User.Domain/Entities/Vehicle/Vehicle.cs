﻿namespace Drivalo.User.Domain
{
    using System.Collections.Generic;

    using Drivalo.Common.Domain;

    public class Vehicle : EntityBase<long>
    {
        public virtual int Year { get; set; }
        public virtual string Model { get; set; } = default!;

        public virtual ShifterType ShifterType { get; set; } = default!;
        public virtual FuelType FuelType { get; set; } = default!;
        public virtual IEnumerable<VehicleVehicleFeature>? VehicleFeatures { get; set; }
    }
}