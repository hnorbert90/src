﻿namespace Drivalo.User.Domain
{
    using Drivalo.Common.Domain;

    public class VehicleVehicleFeature : EntityBase<long>
    {
        public virtual int VehicleFeatureID { get; set; }
        public virtual long VehicleID { get; set; }
        public virtual VehicleFeature? VehicleFeature { get; set; }
        public virtual Vehicle? Vehicle { get; set; }
    }
}