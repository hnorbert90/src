﻿namespace Drivalo.User.Domain
{
    using Drivalo.Common.Domain;

    public class VehicleFeature : EntityBase<int>
    {
        public virtual string Name { get; set; } = default!;
    }
}