﻿namespace Drivalo.User.API
{
    using System.Linq;
    using System.Threading.Tasks;

    using Drivalo.User.Business.Requests;
    using Drivalo.User.Persistence;

    using MediatR;

    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserDbContext _context;
        private readonly IMediator mediator;

        public UsersController(UserDbContext context, SeedDB seed, IMediator mediator)
        {
            _context = context;
            this.mediator = mediator;
            if ( !context.Users.Any() )
                seed.SeedUser();
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult> GetUsers()
        {
            var result = await mediator.Send(new ListUsersQuery());
            return new JsonResult(result);
        }

        //// GET: api/Users/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<User>> GetUser(long id)
        //{
        //    var user = await _context.Users.FindAsync(id);

        //    if ( user == null )
        //    {
        //        return NotFound();
        //    }

        //    return user;
        //}

        //// PUT: api/Users/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for
        //// more details see https://aka.ms/RazorPagesCRUD.
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutUser(long id, User user)
        //{
        //    if ( id != user.ID )
        //    {
        //        return BadRequest();
        //    }

        //    _context.Users.Update(user);

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch ( DbUpdateConcurrencyException )
        //    {
        //        if ( !UserExists(id) )
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        //// POST: api/Users
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for
        //// more details see https://aka.ms/RazorPagesCRUD.
        //[HttpPost]
        //public async Task<ActionResult<User>> PostUser(User user)
        //{
        //    user.Modified = user.Created = DateTime.Now;
        //    user.PrivacyPolicyAccepted = false;
        //    user.VerifiedEmail = false;

        //    _context.Users.Add(user);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetUser", new { id = user.ID }, user);
        //}

        //// DELETE: api/Users/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<User>> DeleteUser(long id)
        //{
        //    var user = await _context.Users.FindAsync(id);
        //    if ( user == null )
        //    {
        //        return NotFound();
        //    }

        //    _context.Users.Remove(user);
        //    await _context.SaveChangesAsync();

        //    return user;
        //}

        //private bool UserExists(long id)
        //{
        //    return _context.Users.Any(e => e.ID == id);
        //}
    }
}
