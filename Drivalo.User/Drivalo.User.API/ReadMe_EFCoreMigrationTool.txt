﻿Parancsok futtatásához telepiteni kell a következő nuget csomagot a startup projektbe: Microsoft.EntityFrameworkCore.Tools (3.1.2)
Default projektet a Persistence projektre kell állítani a nuget package console-ban!

Migrációs lépés hozzáadása: 
add-migration -name <Migrációs lépés elnevezése> -context <db context neve> -OutputDir <migrációs fájlok helye> -StartupProject <startup project neve>
pl: Add-Migration -Name Initial_Create -Context DrivaloDbContext -OutputDir Migrations\MySQL -StartupProject Drivalo.User.API

Adatbázis frissitése:
Update-Database

Utolsó migrációs lépés törlése:
Remove-Migration -Context DrivaloDbContext -StartupProject Drivalo.User.API
