﻿namespace Drivalo.User.CrossCutting
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;

    using Drivalo.User.Persistence;

    using Microsoft.Extensions.Configuration;

    public class Persistence : IWindsorInstaller
    {
        private readonly IConfiguration configuration;
        private readonly IDatabaseSettings databaseSettings;

        public Persistence(IConfiguration configuration, IDatabaseSettings databaseSettings)
        {
            this.configuration = configuration;
            this.databaseSettings = databaseSettings;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<UserDbContext>().UsingFactoryMethod(() => new UserDbContext(new DbContextOptionsFactory<UserDbContext>(databaseSettings).Create())).LifestyleTransient());
            container.Register(Component.For<IDatabaseSettings>().UsingFactoryMethod(() => new DatabaseSettings(configuration)));
            container.Register(Component.For<SeedDB>().ImplementedBy<SeedDB>().LifestyleTransient());
        }
    }
}