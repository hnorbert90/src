﻿namespace Drivalo.Messages.Persistence
{
    using Drivalo.Messages.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class MessageConfiguration : IEntityTypeConfiguration<Message>
    {
        public void Configure(EntityTypeBuilder<Message> builder)
        {
            builder.HasKey(message => message.ID);
            builder.Property(message => message.ID).ValueGeneratedOnAdd();

            builder.Property(message => message.Content);
            builder.Property(message => message.Important);
            builder.Property(message => message.Deleted);
            builder.Property(message => message.Seen);
            builder.Property(message => message.Subject);
        }
    }
}