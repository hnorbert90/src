﻿namespace Drivalo.Messages.Domain
{
    using Microsoft.EntityFrameworkCore;

    public interface IMessageDbContext
    {
        DbSet<Message> Messages { get; set; }
    }
}