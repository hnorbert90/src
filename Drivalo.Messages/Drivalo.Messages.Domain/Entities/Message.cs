﻿namespace Drivalo.Messages.Domain
{
    using Drivalo.Common.Domain;

    public class Message : EntityBase<long>
    {
        public virtual string Subject { get; set; } = default!;
        public virtual string Content { get; set; } = default!;
        public virtual bool Deleted { get; set; }
        public virtual bool Seen { get; set; }
        public virtual bool Important { get; set; }
    }
}