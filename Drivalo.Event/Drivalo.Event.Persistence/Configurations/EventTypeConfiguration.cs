﻿namespace Drivalo.User.Persistence
{
    using Drivalo.Event.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class EventTypeConfiguration : IEntityTypeConfiguration<EventType>
    {
        public void Configure(EntityTypeBuilder<EventType> builder)
        {
            builder.HasKey(eventType => eventType.ID);
            builder.Property(eventType => eventType.ID).ValueGeneratedOnAdd();
            builder.HasIndex(eventType => eventType.Name).IsUnique();

            builder.Property(eventType => eventType.Name).HasMaxLength(100);
        }
    }
}