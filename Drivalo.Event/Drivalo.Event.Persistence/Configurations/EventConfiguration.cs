﻿namespace Drivalo.Event.Persistence
{
    using Drivalo.Event.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class EventConfiguration : IEntityTypeConfiguration<Event>
    {
        public void Configure(EntityTypeBuilder<Event> builder)
        {
            builder.HasKey(@event => @event.ID);
            builder.Property(@event => @event.ID).ValueGeneratedOnAdd();

            builder.Property(@event => @event.EventTypeID);
            builder.Property(@event => @event.LocationPointID);
            builder.Property(@event => @event.Description).HasMaxLength(100);
            builder.Property(@event => @event.Start);
            builder.Property(@event => @event.End);


            builder.HasOne(entity => entity.EventType).WithOne().HasForeignKey<Event>(entity => entity.EventTypeID);
        }
    }
}