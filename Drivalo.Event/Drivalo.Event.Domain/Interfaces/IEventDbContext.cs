﻿namespace Drivalo.Event.Domain
{
    using Microsoft.EntityFrameworkCore;

    public interface IEventDbContext
    {
        DbSet<Event> Events { get; set; }
        DbSet<EventType> EventTypes { get; set; }
    }
}