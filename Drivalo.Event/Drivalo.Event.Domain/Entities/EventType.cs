﻿namespace Drivalo.Event.Domain
{
    using Drivalo.Common.Domain;

    public class EventType : EntityBase<int>
    {
        public virtual string Name { get; set; } = default!;
    }
}