﻿namespace Drivalo.Event.Domain
{
    using System;

    using Drivalo.Common.Domain;

    public class Event : EntityBase<long>
    {
        public virtual int EventTypeID { get; set; }
        public virtual DateTime Start { get; set; }
        public virtual DateTime End { get; set; }
        public virtual string Description { get; set; } = default!;
        public virtual long LocationPointID { get; set; }

        public virtual EventType? EventType { get; set; }
    }
}