import Vue from 'vue';
import Router from 'vue-router';
import Index from './pages/Index.vue';
import Profile from './pages/Profile/Profile.vue';
import ELearningPage from './pages/ELearningPage.vue';
import HighWayCodePage from './pages/HighWayCodePage.vue';
import FirstAidPage from './pages/FirstAidPage.vue';
import GyikPage from './pages/GyikPage.vue';
import InstructorsPage from './pages/InstructorsPage.vue';
import MainNavbar from './layout/MainNavbar.vue';
import MainFooter from './layout/MainFooter.vue';

Vue.use(Router);

export default new Router({
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/',
      name: 'index',
      components: { default: Index, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: 'app-default' }
      }
    },
    {
      path: '/profile',
      name: 'profile',
      components: { default: Profile, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: 'app-default' }
      }
    },
    {
      path: '/e-learning',
      name: 'e-learning',
      components: { default: ELearningPage, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: 'app-default' }
      }
    },
    {
      path: '/gyik',
      name: 'gyik',
      components: { default: GyikPage, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: 'app-default' }
      }
    },
    {
      path: '/instructors',
      name: 'instructors',
      components: { default: InstructorsPage, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: 'app-default' }
      }
    },
    {
      path: '/highway-code',
      name: 'highway-code',
      components: { default: HighWayCodePage, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: 'app-default' }
      }
    },
    {
      path: '/firstAid',
      name: 'firstAid',
      components: { default: FirstAidPage, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: 'app-default' }
      }
    }
  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});
