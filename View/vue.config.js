module.exports = {
  css: {
    // Enable CSS source maps. 
    sourceMap: process.env.NODE_ENV !== 'production'
  },
  /* The base URL your application bundle will be deployed at (known as baseUrl before Vue CLI 3.3).
  https://cli.vuejs.org/config/#publicpath */
  publicPath: '',
  /* The directory where the production build files will be generated in when running vue-cli-service build.
   Note the target directory will be removed before building. 
   https://cli.vuejs.org/config/#outputdir */
  outputDir: '../drivalo.ui/drivalo.ui.web/wwwroot',
  /* A directory (relative to outputDir) to nest generated static assets (js, css, img, fonts) under.
  https://cli.vuejs.org/config/#assetsdir */
  assetsDir: '',
  /* Specify the output path for the generated index.html (relative to outputDir). Can also be an absolute path.
  https://cli.vuejs.org/config/#indexpath */
  indexPath: 'index.html',
  /* This will tell the dev server to proxy any unknown requests (requests that did not match a static file) to http://localhost:4000. */
  /*   devServer: {
    proxy: 'https://localhost:4000'
  } */
  devServer: {
    disableHostCheck: true
  }
};