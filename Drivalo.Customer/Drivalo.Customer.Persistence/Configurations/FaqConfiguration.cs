﻿namespace Drivalo.Customer.Persistence
{
    using Drivalo.Customer.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class FaqConfiguration : IEntityTypeConfiguration<Faq>
    {
        public void Configure(EntityTypeBuilder<Faq> builder)
        {
            builder.HasKey(p => p.ID);
            builder.Property(p => p.ID).ValueGeneratedOnAdd();
            builder.Property(p => p.CategoryID);
            builder.Property(p => p.Question);
            builder.Property(p => p.Answer);

            builder.HasOne(p => p.Category).WithOne().HasForeignKey<Faq>(p => p.CategoryID);
        }
    }
}