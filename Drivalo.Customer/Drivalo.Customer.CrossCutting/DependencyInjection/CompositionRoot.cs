﻿namespace Drivalo.Customer.CrossCutting
{
    using System;

    using Castle.Windsor;
    using Castle.Windsor.MsDependencyInjection;

    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class CompositionRoot : IServiceProviderFactory<IWindsorContainer>
    {
        private IServiceCollection services = default!;
        private IWindsorContainer windsorContainer = default!;

        public CompositionRoot(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IWindsorContainer CreateBuilder(IServiceCollection services)
        {
            this.services = services;
            windsorContainer = new WindsorContainer();

            return windsorContainer;
        }

        public IServiceProvider CreateServiceProvider(IWindsorContainer containerBuilder)
        {
            containerBuilder.Install(
                new CrossCutting(Configuration),
                new Persistence(Configuration),
                new Business());

            return WindsorRegistrationHelper.CreateServiceProvider(containerBuilder, services);
        }
    }
}