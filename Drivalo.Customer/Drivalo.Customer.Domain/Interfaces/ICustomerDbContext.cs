﻿namespace Drivalo.Customer.Domain
{
    using Microsoft.EntityFrameworkCore;

    public interface ICustomerDbContext
    {
        DbSet<Faq> Faqs { get; set; }
        DbSet<FaqCategory> FaqCategories { get; set; }
    }
}