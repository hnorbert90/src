﻿namespace Drivalo.Customer.Domain
{
    using Drivalo.Common.Domain;

    public class Faq : EntityBase<int>
    {
        public virtual int CategoryID { get; set; }
        public virtual string Question { get; set; } = default!;
        public virtual string Answer { get; set; } = default!;

        public virtual FaqCategory? Category { get; set; }
    }
}