﻿namespace Drivalo.Customer.Domain
{
    using Drivalo.Common.Domain;

    public class FaqCategory : EntityBase<int>
    {
        public virtual string Description { get; set; } = default!;
    }
}