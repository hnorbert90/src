﻿namespace Drivalo.Office.Persistence
{
    using Drivalo.Office.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class TrainingContractConfiguration : IEntityTypeConfiguration<TrainingContract>
    {
        public void Configure(EntityTypeBuilder<TrainingContract> builder)
        {
            builder.HasKey(contract => contract.ID);
            builder.Property(contract => contract.ID).ValueGeneratedOnAdd();

            builder.Property(contract => contract.BusinessHours).HasMaxLength(150);
            builder.Property(contract => contract.CashPaymentBill);
            builder.Property(contract => contract.ClientAddress).HasMaxLength(255);
            builder.Property(contract => contract.ClientBirthDay);
            builder.Property(contract => contract.ClientBirthPlace).HasMaxLength(255);
            builder.Property(contract => contract.ClientCompanyAddress).HasMaxLength(255);
            builder.Property(contract => contract.ClientCompanyName).HasMaxLength(255);
            builder.Property(contract => contract.ClientIdentityCardNumber).HasMaxLength(20);
            builder.Property(contract => contract.ClientMaidenName).HasMaxLength(100);
            builder.Property(contract => contract.ClientMotherMaidenName).HasMaxLength(100);
            builder.Property(contract => contract.ClientName).HasMaxLength(100);
            builder.Property(contract => contract.CompanyCourtRegistrationNumber).HasMaxLength(255);
            builder.Property(contract => contract.CompanyName).HasMaxLength(255);
            builder.Property(contract => contract.CompanyRegistrationNumber).HasMaxLength(30);
            builder.Property(contract => contract.ContractTypeID);
            builder.Property(contract => contract.Created);
            builder.Property(contract => contract.Email).HasMaxLength(255);
            builder.Property(contract => contract.HighwayCodeCourseEnd);
            builder.Property(contract => contract.HighwayCodeCourseStart);
            builder.Property(contract => contract.HighwayCodeExamFee);
            builder.Property(contract => contract.KSHNumber).HasMaxLength(50);
            builder.Property(contract => contract.LegalPerson).HasMaxLength(100);
            builder.Property(contract => contract.Mobile).HasMaxLength(50);
            builder.Property(contract => contract.Office).HasMaxLength(255);
            builder.Property(contract => contract.Phone).HasMaxLength(50);
            builder.Property(contract => contract.TaxNumber).HasMaxLength(50);
            builder.Property(contract => contract.TheoreticalEducationTypeID);
            builder.Property(contract => contract.TrafficExamFee);
            builder.Property(contract => contract.TrainingPermitNumber).HasMaxLength(100);
            builder.Property(contract => contract.UserID);
            builder.Property(contract => contract.ValidityStart);
            builder.Property(contract => contract.ValidityEnd);
            builder.Property(contract => contract.Website).HasMaxLength(255);

            builder.HasOne(contract => contract.ContractType).WithOne().HasForeignKey<TrainingContract>(contract => contract.ContractTypeID);
        }
    }
}