﻿namespace Drivalo.Office.Persistence
{
    using Drivalo.Office.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ContractTypeConfiguration : IEntityTypeConfiguration<ContractType>
    {
        public void Configure(EntityTypeBuilder<ContractType> builder)
        {
            builder.HasKey(eventType => eventType.ID);
            builder.Property(eventType => eventType.ID).ValueGeneratedOnAdd();

            builder.Property(eventType => eventType.Name).HasMaxLength(100);
        }
    }
}