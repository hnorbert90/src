﻿namespace Drivalo.Office.Persistence
{
    using Drivalo.Office.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class TheoreticalEducationTypeConfiguration : IEntityTypeConfiguration<TheoreticalEducationType>
    {
        public void Configure(EntityTypeBuilder<TheoreticalEducationType> builder)
        {
            builder.HasKey(eventType => eventType.ID);
            builder.Property(eventType => eventType.ID).ValueGeneratedOnAdd();

            builder.Property(eventType => eventType.Name).HasMaxLength(100);
        }
    }
}