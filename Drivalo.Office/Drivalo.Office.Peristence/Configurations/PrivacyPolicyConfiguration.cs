﻿namespace Drivalo.Office.Persistence
{
    using Drivalo.Office.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class PrivacyPolicyConfiguration : IEntityTypeConfiguration<PrivacyPolicy>
    {
        public void Configure(EntityTypeBuilder<PrivacyPolicy> builder)
        {
            builder.HasKey(privacyPolicy => privacyPolicy.ID);
            builder.Property(privacyPolicy => privacyPolicy.ID).ValueGeneratedOnAdd();
            builder.HasIndex(privacyPolicy => privacyPolicy.Description).IsUnique();

            builder.Property(privacyPolicy => privacyPolicy.Description).HasMaxLength(255);
        }
    }
}