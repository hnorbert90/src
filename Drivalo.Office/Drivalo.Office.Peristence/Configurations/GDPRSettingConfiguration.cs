﻿namespace Drivalo.Office.Persistence
{
    using Drivalo.Office.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class GDPRSettingConfiguration : IEntityTypeConfiguration<GDPRSetting>
    {
        public void Configure(EntityTypeBuilder<GDPRSetting> builder)
        {
            builder.HasKey(setting => setting.ID);
            builder.Property(setting => setting.ID).ValueGeneratedOnAdd();

            builder.Property(setting => setting.UserID);
            builder.Property(setting => setting.PrivacyPolicyID);
            builder.Property(setting => setting.Accepted);

            builder.HasOne(setting => setting.PrivacyPolicy).WithOne().HasForeignKey<GDPRSetting>(setting => setting.PrivacyPolicyID);
        }
    }
}