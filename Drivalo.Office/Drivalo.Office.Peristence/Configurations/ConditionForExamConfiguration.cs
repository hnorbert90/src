﻿namespace Drivalo.Office.Persistence
{
    using Drivalo.Office.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ConditionForExamConfiguration : IEntityTypeConfiguration<ConditionForExam>
    {
        public void Configure(EntityTypeBuilder<ConditionForExam> builder)
        {
            builder.HasKey(eventType => eventType.ID);
            builder.Property(eventType => eventType.ID).ValueGeneratedOnAdd();

            builder.Property(eventType => eventType.Condition).HasMaxLength(255);
        }
    }
}