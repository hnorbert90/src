﻿namespace Drivalo.Office.Persistence
{
    using Drivalo.Office.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ExamLocationConfiguration : IEntityTypeConfiguration<ExamLocation>
    {
        public void Configure(EntityTypeBuilder<ExamLocation> builder)
        {
            builder.HasKey(loc => loc.ID);
            builder.Property(loc => loc.ID).ValueGeneratedOnAdd();

            builder.Property(loc => loc.Description);
            builder.Property(loc => loc.LocationID);
        }
    }
}