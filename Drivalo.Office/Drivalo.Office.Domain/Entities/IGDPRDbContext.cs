﻿namespace Drivalo.Office.Domain
{
    using Microsoft.EntityFrameworkCore;

    public interface IGDPRDbContext
    {
        DbSet<GDPRSetting> GDPRSettings { get; set; }
        DbSet<PrivacyPolicy> PrivacyPolicies { get; set; }
    }
}