﻿namespace Drivalo.Office.Domain
{
    using System;
    using System.Collections.Generic;

    using Drivalo.Common.Domain;

    public class TrainingContract : EntityBase<long>
    {
        public virtual long UserID { get; set; }
        public virtual int ContractTypeID { get; set; }
        public virtual string CompanyName { get; set; } = default!;
        public virtual string Office { get; set; } = default!;
        public virtual string LegalPerson { get; set; } = default!;
        public virtual string Phone { get; set; } = default!;
        public virtual string Mobile { get; set; } = default!;
        public virtual string Website { get; set; } = default!;
        public virtual string Email { get; set; } = default!;
        public virtual string BusinessHours { get; set; } = default!;
        public virtual string CompanyRegistrationNumber { get; set; } = default!;
        public virtual string TrainingPermitNumber { get; set; } = default!;
        public virtual string TaxNumber { get; set; } = default!;
        public virtual string KSHNumber { get; set; } = default!;
        public virtual string CompanyCourtRegistrationNumber { get; set; } = default!;
        public virtual string ClientName { get; set; } = default!;
        public virtual string ClientIdentityCardNumber { get; set; } = default!;
        public virtual string ClientMotherMaidenName { get; set; } = default!;
        public virtual string ClientBirthPlace { get; set; } = default!;
        public virtual DateTime ClientBirthDay { get; set; }
        public virtual string ClientAddress { get; set; } = default!;
        public virtual string ClientMaidenName { get; set; } = default!;
        public virtual string? ClientCompanyName { get; set; }
        public virtual string? ClientCompanyAddress { get; set; }
        public virtual bool CashPaymentBill { get; set; }
        public virtual int LicenseCategoryID { get; set; }
        public virtual int TheoreticalEducationTypeID { get; set; }
        public virtual DateTime HighwayCodeCourseStart { get; set; }
        public virtual DateTime HighwayCodeCourseEnd { get; set; }
        public virtual double HighwayCodeExamFee { get; set; }
        public virtual double TrafficExamFee { get; set; }
        public virtual DateTime Created { get; set; }
        public virtual DateTime ValidityStart { get; set; }
        public virtual DateTime? ValidityEnd { get; set; }

        public virtual IEnumerable<ExamLocation>? ExamLocations { get; set; }
        public virtual IEnumerable<ConditionForExam>? ConditionsForExam { get; set; }
        public virtual ContractType? ContractType { get; set; }
    }
}