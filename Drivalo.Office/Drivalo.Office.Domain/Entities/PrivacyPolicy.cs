﻿namespace Drivalo.Office.Domain
{
    using Drivalo.Common.Domain;

    public class PrivacyPolicy : EntityBase<int>
    {
        public virtual string Description { get; set; } = default!;
    }
}