﻿namespace Drivalo.Office.Domain
{
    using Drivalo.Common.Domain;

    public class GDPRSetting : EntityBase<int>
    {
        public virtual long UserID { get; set; }
        public virtual int PrivacyPolicyID { get; set; }
        public virtual bool Accepted { get; set; }

        public virtual PrivacyPolicy? PrivacyPolicy { get; set; }
    }
}