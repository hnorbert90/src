﻿namespace Drivalo.Office.Domain
{
    using Drivalo.Common.Domain;

    public class ConditionForExam : EntityBase<int>
    {
        public virtual long TrainingContractID { get; set; }
        public virtual string Condition { get; set; } = default!;
    }
}