﻿namespace Drivalo.Office.Domain
{
    using Drivalo.Common.Domain;

    public class TheoreticalEducationType : EntityBase<int>
    {
        public virtual string Name { get; set; } = default!;
    }
}