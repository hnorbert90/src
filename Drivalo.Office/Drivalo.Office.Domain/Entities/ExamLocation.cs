﻿namespace Drivalo.Office.Domain
{
    using Drivalo.Common.Domain;

    public class ExamLocation : EntityBase<int>
    {
        public virtual long TrainingContractID { get; set; }
        public virtual long LocationID { get; set; }
        public virtual string Description { get; set; } = default!;

        public virtual TrainingContract? TrainingContract { get; set; }
    }
}