﻿namespace Drivalo.Office.Domain
{
    using Drivalo.Common.Domain;

    public class ContractType : EntityBase<int>
    {
        public virtual string Name { get; set; } = default!;
    }
}