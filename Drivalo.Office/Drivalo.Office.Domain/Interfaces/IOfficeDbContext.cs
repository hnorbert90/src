﻿namespace Drivalo.Office.Domain
{
    using Microsoft.EntityFrameworkCore;

    public interface IOfficeDbContext
    {
        DbSet<ConditionForExam> ConditionsForExam { get; set; }
        DbSet<ContractType> ContractTypes { get; set; }
        DbSet<ExamLocation> ExamLocations { get; set; }
        DbSet<TheoreticalEducationType> TheoreticalEducationTypes { get; set; }
        DbSet<TrainingContract> TrainingContracts { get; set; }
    }
}