﻿namespace Drivalo.Management.CrossCutting
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;

    using Microsoft.Extensions.Configuration;

    public class Persistence : IWindsorInstaller
    {
        private readonly IConfiguration configuration;

        public Persistence(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
        }
    }
}