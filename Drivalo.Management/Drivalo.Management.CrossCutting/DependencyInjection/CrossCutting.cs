﻿namespace Drivalo.Management.CrossCutting
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;

    using Microsoft.Extensions.Configuration;

    public class CrossCutting : IWindsorInstaller
    {
        private IConfiguration configuration;

        public CrossCutting(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
        }
    }
}