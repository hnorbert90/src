﻿namespace Drivalo.Product.Domain
{
    using Microsoft.EntityFrameworkCore;

    public interface IProductDbContext
    {
        DbSet<Payment> Payments { get; set; }
        DbSet<Product> Products { get; set; }
        DbSet<ProductType> ProductTypes { get; set; }
        DbSet<Feature> Features { get; set; }
    }
}