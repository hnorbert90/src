﻿namespace Drivalo.Product.Domain
{
    using System.Collections.Generic;

    using Drivalo.Common.Domain;

    public class Feature : EntityBase<int>
    {
        public virtual string Name { get; set; } = default!;
        public virtual string Description { get; set; } = default!;

        public virtual IEnumerable<ProductFeature>? ProductFeatures { get; set; }
    }
}