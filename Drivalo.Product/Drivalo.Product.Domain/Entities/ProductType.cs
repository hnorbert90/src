﻿namespace Drivalo.Product.Domain
{
    using Drivalo.Common.Domain;

    public class ProductType : EntityBase<int>
    {
        public virtual string Name { get; set; } = default!;
    }
}