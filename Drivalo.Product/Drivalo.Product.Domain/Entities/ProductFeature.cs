﻿namespace Drivalo.Product.Domain
{
    using Drivalo.Common.Domain;

    public class ProductFeature : EntityBase<long>
    {
        public virtual int FeatureID { get; set; }
        public virtual long ProductID { get; set; }

        public virtual Feature? Feature { get; set; }
        public virtual Product? Product { get; set; }
    }
}