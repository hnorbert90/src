﻿namespace Drivalo.Product.Domain
{
    using Drivalo.Common.Domain;

    public class Payment : EntityBase<long>
    {
        public virtual long UserID { get; set; }
        public virtual long ProductID { get; set; }
        public virtual double Amount { get; set; }
        public virtual Product? Product { get; set; }
    }
}