﻿namespace Drivalo.Product.Domain
{
    using System.Collections.Generic;

    using Drivalo.Common.Domain;

    public class Product : EntityBase<long>
    {
        public virtual string Name { get; set; } = default!;
        public virtual int ProductTypeID { get; set; }
        public virtual string Description { get; set; } = default!;
        public virtual double Price { get; set; }

        public virtual ProductType? ProductType { get; set; }
        public virtual IEnumerable<ProductFeature>? ProductFeatures { get; set; }
    }
}