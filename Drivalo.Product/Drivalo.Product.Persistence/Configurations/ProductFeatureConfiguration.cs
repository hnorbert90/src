﻿namespace Drivalo.Product.Persistence
{
    using Drivalo.Product.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ProductFeatureConfiguration : IEntityTypeConfiguration<ProductFeature>
    {
        public void Configure(EntityTypeBuilder<ProductFeature> builder)
        {
            builder.HasIndex(t => new { t.FeatureID, t.ProductID });

            builder.HasKey(productFeature => productFeature.ID);
            builder.Property(productFeature => productFeature.ID).ValueGeneratedOnAdd();

            builder.Property(productFeature => productFeature.FeatureID);
            builder.Property(productFeature => productFeature.ProductID);

            builder.HasOne(productFeature => productFeature.Product).WithMany(product => product!.ProductFeatures).HasForeignKey(productFeature => productFeature.ProductID);
            builder.HasOne(productFeature => productFeature.Feature).WithMany(feature => feature!.ProductFeatures!).HasForeignKey(productFeature => productFeature.FeatureID);
        }
    }
}