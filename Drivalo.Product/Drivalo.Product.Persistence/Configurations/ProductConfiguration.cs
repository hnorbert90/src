﻿namespace Drivalo.Product.Persistence
{
    using Drivalo.Product.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasKey(product => product.ID);
            builder.Property(product => product.ID).ValueGeneratedOnAdd();

            builder.Property(product => product.ProductTypeID);
            builder.Property(product => product.Price);
            builder.Property(product => product.Name).HasMaxLength(150);
            builder.Property(product => product.Description).HasMaxLength(255);

            builder.HasOne(product => product.ProductType).WithOne().HasForeignKey<Product>(product => product.ProductTypeID);
        }
    }
}