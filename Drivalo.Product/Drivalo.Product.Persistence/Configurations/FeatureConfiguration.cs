﻿namespace Drivalo.Product.Persistence
{
    using Drivalo.Product.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class FeatureConfiguration : IEntityTypeConfiguration<Feature>
    {
        public void Configure(EntityTypeBuilder<Feature> builder)
        {
            builder.HasKey(feature => feature.ID);
            builder.Property(feature => feature.ID).ValueGeneratedOnAdd();
            builder.HasIndex(feature => feature.Name).IsUnique();
            builder.Property(feature => feature.Name).HasMaxLength(100);
            builder.Property(feature => feature.Description).HasMaxLength(255);
        }
    }
}