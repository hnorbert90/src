﻿namespace Drivalo.Product.Persistence
{
    using Drivalo.Product.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class PaymentConfiguration : IEntityTypeConfiguration<Payment>
    {
        public void Configure(EntityTypeBuilder<Payment> builder)
        {
            builder.HasKey(payment => payment.ID);
            builder.Property(payment => payment.ID).ValueGeneratedOnAdd();

            builder.Property(payment => payment.ProductID);
            builder.Property(payment => payment.UserID);
            builder.Property(payment => payment.Amount);

            builder.HasOne(payment => payment.Product).WithOne().HasForeignKey<Payment>(payment => payment.ProductID);
        }
    }
}