﻿namespace Drivalo.Product.Persistence
{
    using Drivalo.Product.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ProductTypeConfiguration : IEntityTypeConfiguration<ProductType>
    {
        public void Configure(EntityTypeBuilder<ProductType> builder)
        {
            builder.HasKey(productType => productType.ID);
            builder.Property(productType => productType.ID).ValueGeneratedOnAdd();
            builder.HasIndex(productType => productType.Name).IsUnique();

            builder.Property(productType => productType.Name).HasMaxLength(100);
        }
    }
}