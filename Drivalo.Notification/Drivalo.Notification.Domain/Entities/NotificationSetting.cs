﻿namespace Drivalo.Notification.Domain
{
    using Drivalo.Common.Domain;

    public class NotificationSetting : EntityBase<int>
    {
        public virtual long UserID { get; set; }
        public virtual int NotificationTypeID { get; set; }
        public virtual bool Value { get; set; } = default!;

        public virtual NotificationType? NotificationType { get; set; }
    }
}