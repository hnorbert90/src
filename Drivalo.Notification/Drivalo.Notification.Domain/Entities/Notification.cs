﻿namespace Drivalo.Notification.Domain
{
    using System;

    using Drivalo.Common.Domain;


    public class Notification : EntityBase<long>
    {
        public virtual int NotificationTypeID { get; set; }
        public virtual string Content { get; set; } = default!;
        public virtual DateTime Created { get; set; }
        public virtual bool Important { get; set; }
        public virtual bool Seen { get; set; }
        public virtual NotificationType? NotificationType { get; set; }
    }
}