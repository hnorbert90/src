﻿namespace Drivalo.Notification.Domain
{
    using Drivalo.Common.Domain;

    public class NotificationType : EntityBase<int>
    {
        public virtual string Description { get; set; } = default!;
    }
}