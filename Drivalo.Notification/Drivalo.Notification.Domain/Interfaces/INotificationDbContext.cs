﻿namespace Drivalo.Notification.Domain
{
    using Microsoft.EntityFrameworkCore;

    public interface INotificationDbContext
    {
        DbSet<Notification> Notifications { get; set; }
        DbSet<NotificationSetting> NotificationSettings { get; set; }
        DbSet<NotificationType> NotificationTypes { get; set; }
    }
}