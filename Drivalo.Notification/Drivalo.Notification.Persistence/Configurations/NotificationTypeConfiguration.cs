﻿namespace Drivalo.Notification.Persistence
{
    using Drivalo.Notification.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class NotificationTypeConfiguration : IEntityTypeConfiguration<NotificationType>
    {
        public void Configure(EntityTypeBuilder<NotificationType> builder)
        {
            builder.HasKey(notificationType => notificationType.ID);
            builder.Property(notificationType => notificationType.ID).ValueGeneratedOnAdd();

            builder.Property(notificationType => notificationType.Description).HasMaxLength(255);
        }
    }
}