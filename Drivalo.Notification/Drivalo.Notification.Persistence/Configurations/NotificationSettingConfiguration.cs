﻿namespace Drivalo.Notification.Persistence
{
    using Drivalo.Notification.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class NotificationSettingConfiguration : IEntityTypeConfiguration<NotificationSetting>
    {
        public void Configure(EntityTypeBuilder<NotificationSetting> builder)
        {
            builder.HasKey(notificationSetting => notificationSetting.ID);
            builder.Property(notificationSetting => notificationSetting.ID).ValueGeneratedOnAdd();

            builder.Property(notificationSetting => notificationSetting.UserID);
            builder.Property(notificationSetting => notificationSetting.NotificationTypeID);
            builder.Property(notificationSetting => notificationSetting.Value);

            builder.HasOne(notificationSetting => notificationSetting.NotificationType).WithOne().HasForeignKey<NotificationSetting>(notificationSetting => notificationSetting.NotificationTypeID);
        }
    }
}