﻿namespace Drivalo.Notification.Persistence
{
    using Drivalo.Notification.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class NotificationConfiguration : IEntityTypeConfiguration<Notification>
    {
        public void Configure(EntityTypeBuilder<Notification> builder)
        {
            builder.HasKey(notification => notification.ID);
            builder.Property(notification => notification.ID).ValueGeneratedOnAdd();

            builder.Property(notification => notification.Content).HasMaxLength(255).IsRequired();
            builder.Property(notification => notification.Important);
            builder.Property(notification => notification.NotificationTypeID);
            builder.Property(notification => notification.Created);
            builder.Property(notification => notification.Seen);

            builder.HasOne(notification => notification.NotificationType).WithOne().HasForeignKey<Notification>(notification => notification.NotificationTypeID);
        }
    }
}