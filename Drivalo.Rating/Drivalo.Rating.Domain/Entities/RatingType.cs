﻿namespace Drivalo.Rating.Domain
{
    using Drivalo.Common.Domain;

    public class RatingType : EntityBase<int>
    {
        public virtual string Name { get; set; } = default!;
    }
}