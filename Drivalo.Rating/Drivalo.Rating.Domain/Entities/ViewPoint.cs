﻿namespace Drivalo.Rating.Domain
{

    using Drivalo.Common.Domain;

    public class ViewPoint : EntityBase<int>
    {
        public virtual string Name { get; set; } = default!;
    }
}