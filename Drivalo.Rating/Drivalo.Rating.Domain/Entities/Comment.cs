﻿namespace Drivalo.Rating.Domain
{
    using Drivalo.Common.Domain;

    public class Comment : EntityBase<long>
    {
        public virtual long SubjectId { get; set; }
        public virtual long AuthorID { get; set; }
        public virtual long RateViewPointID { get; set; }
        public virtual string Content { get; set; } = default!;
        public virtual bool Accepted { get; set; }
    }
}