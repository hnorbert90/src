﻿namespace Drivalo.Rating.Domain
{
    using Drivalo.Common.Domain;

    public class RoleViewPoint : EntityBase<int>
    {
        public virtual int RoleID { get; set; }
        public virtual int ViewPointID { get; set; }
        public virtual ViewPoint? ViewPoint { get; set; }
    }
}