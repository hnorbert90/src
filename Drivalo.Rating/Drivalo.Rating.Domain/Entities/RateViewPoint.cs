﻿namespace Drivalo.Rating.Domain
{
    using Drivalo.Common.Domain;

    public class RateViewPoint : EntityBase<long>
    {
        public virtual long RatingID { get; set; }

        public virtual float Value { get; set; }

        public virtual Rating? Rating { get; set; }
        public virtual Comment? Comment { get; set; }
        public virtual ViewPoint? ViewPoint { get; set; }
    }
}