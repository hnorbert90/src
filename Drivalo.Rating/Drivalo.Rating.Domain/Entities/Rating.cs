﻿namespace Drivalo.Rating.Domain
{
    using System.Collections.Generic;

    using Drivalo.Common.Domain;

    public class Rating : EntityBase<long>
    {
        public virtual int RatingTypeID { get; set; }
        public virtual long UserID { get; set; }

        public virtual IEnumerable<RateViewPoint>? RateViewPoints { get; set; }
        public virtual RatingType? RatingType { get; set; }
    }
}