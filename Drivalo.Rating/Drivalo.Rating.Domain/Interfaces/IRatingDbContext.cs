﻿namespace Drivalo.Rating.Domain
{
    using Microsoft.EntityFrameworkCore;

    public interface IRatingDbContext
    {
        DbSet<Rating> Ratings { get; set; }
        DbSet<RateViewPoint> RateViewPoints { get; set; }
        DbSet<RatingType> RatingTypes { get; set; }
        DbSet<ViewPoint> ViewPoints { get; set; }
        DbSet<Comment> Comments { get; set; }
    }
}