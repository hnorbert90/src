﻿namespace Drivalo.Rating.Persistence
{
    using Drivalo.Rating.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class RatingConfiguration : IEntityTypeConfiguration<Rating>
    {
        public void Configure(EntityTypeBuilder<Rating> builder)
        {
            builder.HasKey(rating => rating.ID);
            builder.Property(rating => rating.ID).ValueGeneratedOnAdd();
            builder.HasIndex(rating => new { rating.RatingTypeID, rating.UserID });

            builder.Property(rating => rating.RatingTypeID);
            builder.Property(rating => rating.UserID);

            builder.HasOne(rating => rating.RatingType).WithOne().HasForeignKey<Rating>(rating => rating.RatingTypeID);
            builder.HasMany(rating => rating.RateViewPoints).WithOne(rateViewPoint => rateViewPoint.Rating!).HasForeignKey(rateViewPoint => rateViewPoint.RatingID);
        }
    }
}