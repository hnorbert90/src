﻿namespace Drivalo.Rating.Persistence
{
    using Drivalo.Rating.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class RatingTypeConfiguration : IEntityTypeConfiguration<RatingType>
    {
        public void Configure(EntityTypeBuilder<RatingType> builder)
        {
            builder.HasKey(ratingType => ratingType.ID);
            builder.Property(ratingType => ratingType.ID).ValueGeneratedOnAdd();
            builder.HasIndex(ratingType => ratingType.Name).IsUnique();

            builder.Property(ratingType => ratingType.Name).HasMaxLength(100);
        }
    }
}