﻿namespace Drivalo.Rating.Persistence
{
    using Drivalo.Rating.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class CommentConfiguration : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.HasKey(comment => comment.ID);
            builder.Property(comment => comment.ID).ValueGeneratedOnAdd();
            builder.HasIndex(comment => new { comment.AuthorID, comment.SubjectId });

            builder.Property(comment => comment.AuthorID);
            builder.Property(comment => comment.SubjectId);
            builder.Property(comment => comment.RateViewPointID);
            builder.Property(comment => comment.Content).HasMaxLength(255);
            builder.Property(comment => comment.Accepted);
        }
    }
}