﻿namespace Drivalo.Rating.Persistence
{
    using Drivalo.Rating.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class RoleViewPointConfiguration : IEntityTypeConfiguration<RoleViewPoint>
    {
        public void Configure(EntityTypeBuilder<RoleViewPoint> builder)
        {
            builder.HasKey(roleViewPoint => roleViewPoint.ID);
            builder.HasIndex(roleViewPoint => new { roleViewPoint.ViewPointID, roleViewPoint.RoleID });
            builder.Property(roleViewPoint => roleViewPoint.ID).ValueGeneratedOnAdd();

            builder.HasIndex(roleViewPoint => roleViewPoint.RoleID);
            builder.HasIndex(roleViewPoint => roleViewPoint.ViewPointID);
        }
    }
}