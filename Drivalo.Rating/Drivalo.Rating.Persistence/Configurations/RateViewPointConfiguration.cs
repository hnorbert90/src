﻿namespace Drivalo.Rating.Persistence
{
    using Drivalo.Rating.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class RateViewPointConfiguration : IEntityTypeConfiguration<RateViewPoint>
    {
        public void Configure(EntityTypeBuilder<RateViewPoint> builder)
        {
            builder.HasKey(rateViewPoint => rateViewPoint.ID);
            builder.Property(rateViewPoint => rateViewPoint.ID).ValueGeneratedOnAdd();

            builder.Property(rateViewPoint => rateViewPoint.Value);
            builder.Property(rateViewPoint => rateViewPoint.RatingID);

            builder.HasOne(rateViewPoint => rateViewPoint.Rating).WithMany(rating => rating!.RateViewPoints!).HasForeignKey(rateViewPoint => rateViewPoint.RatingID);
            builder.OwnsOne(rateViewPoint => rateViewPoint.ViewPoint);
            builder.OwnsOne(rateViewPoint => rateViewPoint.Comment);
        }
    }
}