﻿namespace Drivalo.Rating.Persistence
{
    using Drivalo.Rating.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ViewPointConfiguration : IEntityTypeConfiguration<ViewPoint>
    {
        public void Configure(EntityTypeBuilder<ViewPoint> builder)
        {
            builder.HasKey(viewPoint => viewPoint.ID);
            builder.Property(viewPoint => viewPoint.ID).ValueGeneratedOnAdd();

            builder.HasIndex(viewPoint => viewPoint.Name).IsUnique();

            builder.Property(viewPoint => viewPoint.Name).HasMaxLength(100);
        }
    }
}